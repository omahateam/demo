﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Common.DTO;
using Service.Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private IConfiguration _config;
        public TokenController(ICustomerService customerService, IConfiguration config)
        {
            _customerService = customerService;
            _config = config;
        }

        [HttpPost]
        public IActionResult Post(Customer _userData)
        {

            if (_userData != null && _userData.Email != null && _userData.Password != null)
            {
                var user = _customerService.ValidateCustomer(_userData);

                if (user != null && user.ID > 0)
                {
                    //create claims details based on the user information
                    var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _config["Jwt:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    //new Claim("Id", "1"),
                    //new Claim("FirstName", "test"),
                    //new Claim("LastName", "test"),
                    new Claim("UserName", "test@test.com"),
                    new Claim("Email", user.Email)
                   };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));

                    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(_config["Jwt:Issuer"], _config["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddHours(2), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Invalid credentials");
                }
            }
            else
            {
                return BadRequest("Invalid credentials");
            }
        }

        //private async Task<UserInfo> GetUser(string email, string password)
        //{
        //    return await _context.UserInfo.FirstOrDefaultAsync(u => u.Email == email && u.Password == password);
        //}
    }
}